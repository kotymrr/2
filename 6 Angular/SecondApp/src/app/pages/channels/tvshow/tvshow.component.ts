import { Component, Input } from '@angular/core';
import { Tvshow } from 'src/app/models/channels.model';

@Component({
  selector: 'app-tvshow',
  templateUrl: './tvshow.component.html',
  styleUrls: ['./tvshow.component.scss']
})

export class TvshowComponent {

  @Input() tvshow: Tvshow;

  constructor() {}
}
