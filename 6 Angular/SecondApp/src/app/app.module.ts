import { LoginPageComponent } from './pages/login/login.component';
import { ChannelsPageComponent } from './pages/channels/channels.component';
import { HeaderComponent } from './components/header/header.component';
import { DishComponent } from './components/dish/dish.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrderComponent } from './components/order/order.component';
import { ModalComponent } from './components/modal/modal.component';
import { MenuPageComponent } from './pages/menu/menu.component';
import { RoutingModule } from './routing.module';
import { AuthService } from './services/auth.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from './services/auth.guard';
import { LoginGuard } from './services/login.guard';
import { DataService } from './services/data.service';
import { HttpClientModule } from '@angular/common/http';
import { ChannelComponent } from './pages/channels/channel/channel.component';
import { ScalingSquaresSpinnerModule } from 'angular-epic-spinners';
import { LoadingService } from './services/loading.service';
import { DateComponent } from './pages/channels/date/date.component';
import { DatePipe } from './pipes/date.pipe';
import { TvshowComponent } from './pages/channels/tvshow/tvshow.component';

@NgModule({
  declarations: [
    AppComponent,
    DishComponent,
    OrderComponent,
    ModalComponent,
    HeaderComponent,
    MenuPageComponent,
    LoginPageComponent,
    ChannelsPageComponent,
    ChannelComponent,
    DateComponent,
    DatePipe,
    TvshowComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ScalingSquaresSpinnerModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    LoginGuard,
    DataService,
    LoadingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
