import { Component, Input } from "@angular/core";

@Component({
    selector: 'app-static-button',
    templateUrl: './static-button.component.html',
    styleUrls: ['./static-button.component.scss']
})

export class StaticButtonComponent {

    @Input() counter: number = 0;
    @Input() name: string = '';

    static totalCounter = 0;

    constructor() {}

    public get totalCount() {
        return StaticButtonComponent.totalCounter;
    }

    public onButtonClick() {
        this.counter++;
        StaticButtonComponent.totalCounter++;
    }
}
