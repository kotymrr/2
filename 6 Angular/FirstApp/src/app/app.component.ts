import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public startCounter = 10;

  public buttonName1 = 'Первая кнопка';
  public buttonName2 = 'Вторая кнопка';

  constructor() {}

}
