import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ConditionalExpr } from '@angular/compiler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginPageComponent implements OnInit {

  public name: FormControl;
  public password: FormControl;
  public userForm: FormGroup;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.createFormFields();
    this.createUserForm();
  }

  private createFormFields(): void {
    this.name = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(4)]);
  }

  private createUserForm(): void {
    this.userForm = new FormGroup({
      name: this.name,
      password: this.password
    });
  }

  public temp(): void {
    console.log(this.userForm.value);
  }

  public authorization(): void {
    if (this.userForm.valid) {
      this.authService.login(this.name.value, this.password.value);
      // this.authService.login(this.userForm.value['name'], this.userForm.value['password']);
      const isAuth = this.authService.isLogin;
      if (isAuth) {
        this.router.navigate(['channels']);
      }
    }
  }
}
