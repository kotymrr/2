import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class NotAuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    const isAuth = this.authService.isLogin;
    if (isAuth) {
      this.router.navigate(['channels']);
      return false;
    } else {
      return true;
    }
  }
}
