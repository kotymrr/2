export interface Dish {
  id: number;
  name: string;
  picture: string;
  description: string;
  price: number;
}
