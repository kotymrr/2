import { NotAuthGuard } from './services/notauth.guard';
import { AuthGuard } from './services/auth.guard';
import { LoginPageComponent } from './login/login.component';
import { ChannelsPageComponent } from './pages/channels/channels.component';
import { MenuPageComponent } from './pages/menu/menu.component';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';


const routes: Route[] = [
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full'
  },
  {
    path: 'menu',
    component: MenuPageComponent
  },
  {
    path: 'channels',
    component: ChannelsPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginPageComponent,
    canActivate: [NotAuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class RoutingModule {}
