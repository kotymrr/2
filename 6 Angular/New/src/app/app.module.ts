import { NotAuthGuard } from './services/notauth.guard';
import { AuthGuard } from './services/auth.guard';
import { LoginPageComponent } from './login/login.component';
import { AuthService } from './services/auth.service';
import { ChannelsPageComponent } from './pages/channels/channels.component';
import { HeaderComponent } from './components/header/header.component';
import { DishComponent } from './components/dish/dish.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrderComponent } from './components/order/order.component';
import { ModalComponent } from './components/modal/modal.component';
import { MenuPageComponent } from './pages/menu/menu.component';
import { RoutingModule } from './routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DishComponent,
    OrderComponent,
    ModalComponent,
    HeaderComponent,
    MenuPageComponent,
    ChannelsPageComponent,
    LoginPageComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    NotAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
