var express = require('express');
var app = express();

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var fs = require('fs');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var users = [];

function getUserList() {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/users.json', 'utf-8', (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject();
      }
    });
  });
}

function saveUsers() {
  fs.writeFile('./server-data/users.json', JSON.stringify(users), () => {});
}

function getWorks() {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/works.json', 'utf-8', (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject();
      }
    });
  });
}

function getMasters() {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/masters.json', 'utf-8', (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject();
      }
    });
  });
}

app.get('/', (req, res) => {
  res.send('Api is work!');
});



app.post('/users', (req, res) => {
  const existUser = users.find(us => us.email === req.body.email);
  if (existUser) {
    res.sendStatus(500);
  } else {
    const user = {
      id: Date.now(),
      email: req.body.email,
      password: req.body.password
    };
    users.push(user);
    res.send(user);
    saveUsers();
  }
});

app.post('/auth', (req, res) => {
  const user = req.body;
  const existUser = users.find(u => (u.email === user.email && u.password === user.password));
  if (existUser) {
    res.send(existUser.id.toString());
  } else {
    res.sendStatus(500);
  }
});

app.get('/users', (req, res) => {
  const id = req.query.id;
  const user = users.find(u => +u.id === +id);
  if (user) {
    res.send(JSON.stringify(user.email));
  } else {
    res.sendStatus(500);
  }
});

app.get('/master', (req, res) => {
  const serviceName = req.query.name;
  getWorks().then(works => {
    const findWork = JSON.parse(works).find(work => work.name === serviceName);
    if (findWork) {
      getMasters().then(masters => {
        const result = JSON.parse(masters).filter(m => findWork.masters.includes(m.id));
        if (result) {
          res.send(result);
        } else {
          res.sendStatus(500);
        }
      });
    } else {
      res.sendStatus(500);
    }
  });
});

app.get('/work', (req, res) => {
  const serviceName = req.query.name;
  getWorks().then(works => {
    const findWork = JSON.parse(works).find(work => work.name === serviceName);
    if (findWork) {
      res.send(findWork);
    } else {
      res.sendStatus(500);
    }
  });
});

app.listen(3000, () => {
  console.log('Server started!');
  getUserList().then(data => users = JSON.parse(data));
});
