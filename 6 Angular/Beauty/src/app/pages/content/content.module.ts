import { RouterModule } from '@angular/router';
import { ContentPageComponent } from './content.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullCalendarModule } from '@fullcalendar/angular';

@NgModule({
  declarations: [
    ContentPageComponent
  ],
  imports: [
    CommonModule,
    FullCalendarModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentPageComponent
      }
    ])
  ]
})

export class ContentPageModule {}
