import { Master } from './../../models/data';
import { DataService } from '@services/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import dayGridPlugin from '@fullcalendar/daygrid';

@Component({
  selector: 'app-content-page',
  templateUrl: 'content.component.html',
  styleUrls: ['content.component.scss']
})

export class ContentPageComponent implements OnInit, OnDestroy {

  private routeSubscriber: Subscription;
  private workSubscriber: Subscription;
  private mastersSubscriber: Subscription;

  public masters: Master[] = [];
  private type = 'brows';

  public calendarPlugins = [dayGridPlugin];

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService) {}

  ngOnInit() {
    this.routeSubscriber = this.activatedRoute.params.subscribe(params => {
      this.type = params.type;

      this.workSubscriber = this.dataService.getWork(params.type).subscribe(data => {
        console.log(data);
      });
      this.mastersSubscriber = this.dataService.getMasters(params.type).subscribe(data => {
        this.masters = data;
      });
    });
  }

  /* public get mastersAsync(): Observable<Master[]> {
    return new Observable(observer => {
      this.dataService.getMasters(this.type).subscribe(data => {
        observer.next(data);
      });
    })
  } */

  ngOnDestroy() {
    if (this.routeSubscriber) {
      this.routeSubscriber.unsubscribe();
    }
    if (this.workSubscriber) {
      this.workSubscriber.unsubscribe();
    }
    if (this.mastersSubscriber) {
      this.mastersSubscriber.unsubscribe();
    }
  }
}
