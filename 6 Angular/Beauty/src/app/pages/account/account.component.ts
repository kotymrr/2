import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-page',
  templateUrl: 'account.component.html',
  styleUrls: ['account.component.scss']
})

export class AccountPageComponent implements OnInit {

  public email: string;

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.getInfo();
  }

  public logout(): void {
    this.loginService.logout();
  }

  private getInfo(): void {
    this.loginService.getUserInfo().then(result => {
      this.email = result;

    });
  }
}
