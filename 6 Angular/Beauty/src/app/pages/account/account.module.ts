import { RouterModule } from '@angular/router';
import { AccountPageComponent } from './account.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AccountPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AccountPageComponent
      }
    ])
  ]
})

export class AccountPageModule {}
