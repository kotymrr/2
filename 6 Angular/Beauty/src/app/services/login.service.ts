import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '@models/core';

@Injectable()
export class LoginService {

  public showModalEvent = new Subject<boolean>();

  private readonly url = 'http://localhost:3000/';

  constructor(private http: HttpClient, private router: Router) {}

  showModal(): void {
    this.showModalEvent.next(true);
  }

  closeModal(): void {
    this.showModalEvent.next(false);
  }

  get isLogin(): boolean {
    const token = localStorage.getItem('token');
    if (token) {
      return true;
    }
    return false;
  }

  register(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.post<User>(this.url.concat('users'), user).toPromise().then(res => {
        localStorage.setItem('token', res.id.toString());
        resolve(user.id);
      });
    });
  }

  authorization(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.post(this.url.concat('auth'), user).toPromise().then(res => {
        localStorage.setItem('token', res.toString());
        resolve();
      }).catch(_ => {
        reject();
      });
    });
  }

  getUserInfo(): Promise<string> {
    const id = localStorage.getItem('token');
    const params = new HttpParams().set('id', id);
    return new Promise((resolve, reject) => {
      this.http.get<string>(this.url.concat('users'), { params }).toPromise().then(res => {
        resolve(res);
      }).catch(_ => {
        reject();
      });
    });
  }

  logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['about']);
  }

}

