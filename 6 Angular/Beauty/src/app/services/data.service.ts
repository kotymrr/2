import { Work, Master } from './../models/data';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class DataService {

  private readonly url = 'http://localhost:3000/';

  constructor(private http: HttpClient) {}

  getWork(type: string): Observable<Work> {
    const params = new HttpParams().set('name', type);
    return this.http.get<Work>(this.url.concat('work'), { params });
  }

  getMasters(type: string): Observable<Master[]> {
    const params = new HttpParams().set('name', type);
    return this.http.get<Master[]>(this.url.concat('master'), { params });
  }
}
