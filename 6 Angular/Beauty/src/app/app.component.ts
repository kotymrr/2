import { Component, OnInit } from '@angular/core';
import { LoginService } from '@services/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public isShowModal: boolean;

  constructor(private loginService: LoginService) {}

  ngOnInit() {
    this.loginService.showModalEvent.subscribe(res => {
      this.isShowModal = res;
    });
  }
}
