export interface Master {
  id: number;
  name: string;
  portf: Portfolio;
}

export interface Portfolio {
  text: string;
  images: string[];
}

export interface Work {
  id: number;
  name: string;
  masters: number[];
  time: number;
}

export interface Order {
  order_id: number;
  date: number;
  work_id: number;
  client_id: number;
  options: Option[];
  time: number;
}

export interface Option {
  id: number;
  name: string;
  time: number;
}
