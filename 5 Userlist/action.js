var container = document.querySelector('.container');

var users = loadUsers();
if (users.length>0)
{
    renderAll(users);
}
else
{
    getUsers().then(data => 
        {
        users = data;
        renderAll();
        saveUsers();
        }).catch(error => 
        {
        console.log(error);
        });
}



function getUsers()
{
    return new Promise((resolve, reject)=>
    {
        var xhr = new XMLHttpRequest();
        var data = [];
        xhr.open('GET', 'https://randomuser.me/api/?results=10');
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState === 4)
            {
                if (xhr.status === 200)
                {
                    //console.log(JSON.parse(xhr.responseText).results);
                    data = JSON.parse(xhr.responseText).results;
                    resolve (data);
                }
                else
                {
                    reject('Что-то пошло не так');
                }
            }
        }
        xhr.send();
    });
}

function renderUser(user, index)
{
    container.innerHTML +=`
    <div class="user">
            <img class="avatar" src="${user.picture.thumbnail}" alt="">
            <div class="info">
                <div class="main-ingo">
                    <span class="name">${user.name.first} ${user.name.last}</span>, 
                    <span class="age">${user.dob.age}</span>
                </div>
            <span class="email">${user.email}</span>
            </div>
            <div class="delete" onclick="deleteUser(${index})" >X</div>
        </div>`;
}

function renderAll()
{
    container.innerHTML= '';
    users.forEach((element, index)=>
        {
            renderUser(element, index);
        });
    /*for (var i = 0, i < users.length; i++)
    {
        renderUser(users[i], i)
    }*/
}

function loadUsers()
{
    var users = localStorage.getItem('users');
    if (users)
    {
        return JSON.parse(users);
    }
    else
    {
        return [];
    }
}

function saveUsers ()
{
    localStorage.setItem('users', JSON.stringify(users));
}

function deleteUser(index)
{
    users.splice(index, 1);
    saveUsers();
    renderAll();
}

document.querySelector('.add').addEventListener('click', showModal);
document.querySelector('.save').addEventListener('click', addUser);

function showModal(){
    document.querySelector('.wraper').style.display = 'flex';
}

function addUser(){
    var fullname = document.querySelector('#name').value;
    var age = document.getElementById('age').value;
    var email = document.getElementById('email').value;
    var fullnameArr = fullname.split(' ');
    if (checkName(fullname) && checkEmail(email) && checkAge(age))
    {
        var user = 
        {
            picture: {
                thumbnail:'1.png',
            },
            dob:
            {
                age
            },
            email,
            name:
            {
                first: fullnameArr[0],
                last: fullnameArr[1]
            }
        };
        users.push(user);
        saveUsers();
        renderAll();
        closeModal();
        scrollToNew();
    }
}

function checkAge(age)
{
    if (!isNaN(+age)&& +age>= 18 && +age<=100)
    {
        document.querySelector('#age').classList.remove('error-field');
        return true;
    }
    else
    {
        alert('Введите корректный возраст');
        document.querySelector('#age').classList.add('error-field');
        return false;
    }
}

function checkEmail (email)
{
    var pattern = /^([0-9a-zA-Z_%.-]{1,20}\@[a-zA-Z]{1,10}\.[a-zA-Z]{2,4}$)/;
    if (pattern.test(email))
    {
        document.querySelector('#email').classList.remove('error-field');
        return true;
    }
    else
    {
        alert('Введите корректный email');
        document.querySelector('#email').classList.add('error-field');
        return false;
    }
}

function checkName (name)
{
    var fullnameArr = name.split(' ');
    var pattern = /[A-ZА-Я\W]+/img;
    if (fullnameArr.length>1 && 
        pattern.test(fullnameArr[0]) && 
        pattern.test(fullnameArr[1]))
    {
        document.querySelector('#name').classList.remove('error-field');
        return true;
    }
    else
    {
        alert('Введите корректные имя и фамилию');
        document.querySelector('#name').classList.add('error-field');
        return false;
    }
}

function scrollToNew()
{
    setTimeout(()=>
    {
        var lastUser = document.getElementsByClassName('user')[users.length-1];
        lastUser.scrollIntoView({behavior: 'smooth'});
    }, 0);
}

function closeModal()
{
    document.querySelector ('.wraper').style.display = 'none';
}