//Нажатие на элемент   mousedown
//Перемещение мыши     mousemove
//Отпускание элемента  mouseup
var header = document.querySelector('.header');
var mywindow = document.querySelector('.window');
var headerclose = document.querySelector('.header-close');

var ofX = 0;
var ofY = 0;

header.addEventListener('mousedown', onMouseDown);
document.addEventListener('mouseup', onMouseUp);
headerclose.addEventListener('click', clickEnd);
document.addEventListener('keydown', clickOpen);

function onMouseDown(event)
{
    document.addEventListener('mousemove', onMouseMove);
    ofX = event.offsetX;
    ofY = event.offsetY;
    mywindow.classList.add('window_active');
}
function onMouseMove(event)
{
    mywindow.style.left = event.pageX - ofX +'px';
    mywindow.style.top = event.pageY - ofY +'px';
}
function onMouseUp()
{
    document.removeEventListener('mousemove', onMouseMove);
    mywindow.classList.remove('window_active');
}
function clickEnd()
{
    mywindow.classList.add('window_hide');
}
function clickOpen(event)
{
    console.log(event);
    var code = event.keyCode;
    if (code==32)
    {
        mywindow.classList.remove('window_hide');
    }
}