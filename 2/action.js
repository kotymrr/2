//-----------------Slider 1--------------//
const images = [
    '1.jpg',
    '2.jpg',
    '3.jpg',
    '4.jpg',
    '5.jpg',
];
var currentIndex = 0;
const slider1 = document.getElementsByClassName('slider1-img')[0];
renderImage();

document.getElementById('slider1-prev').addEventListener('click', slide1Prev);
document.getElementById('slider1-next').addEventListener('click', slide1Next);

function renderImage()
{
    slider1.src = './images/' + images[currentIndex];
}

function slide1Next()
{
    if (currentIndex < images.length-1)
    {
        currentIndex++;
    }
    else
    {
        currentIndex=0;
    }
    renderImage();
}

function slide1Prev()
{
    if (currentIndex > 0)
    {
        currentIndex--;
    }
    renderImage();
}
//-----------------Slider 2--------------//
var current2Index=0;
renderAll();
const allImages = document.getElementsByClassName('slider2-img');
showImg();

document.getElementById('slider2-prev').addEventListener('click', slide2Prev);
document.getElementById('slider2-next').addEventListener('click', slide2Next);
function renderAll()
{
    const group = document.getElementsByClassName('slider2-group')[0];
    for (var i = 0; i<images.length; i++)
    {
        const img = document.createElement('img');
        img.src = './images/' + images[i];
        img.classList.add('slider2-img');
        group.appendChild(img);
    }
}
function showImg()
{
    allImages[current2Index].style.opacity = 1;
}

function slide2Prev()
{
    if(current2Index>0)
    {
        allImages[current2Index].style.opacity = 0;
        current2Index--;
        allImages[current2Index].style.opacity = 1;
    }
    else
    {
        allImages[current2Index].style.opacity = 0;
        current2Index=images.length-1;
        allImages[current2Index].style.opacity = 1;
    }
}

function slide2Next()
{
    if(current2Index<images.length-1)
    {
        allImages[current2Index].style.opacity = 0;
        current2Index++;
        allImages[current2Index].style.opacity = 1;
    }
    else
    {
        allImages[current2Index].style.opacity = 0;
        current2Index=images.length-1;
        allImages[current2Index].style.opacity = 1;
    }
}

//-----------------Slider 3--------------//
var current3Index = 0;
document.getElementById('slider3-prev').addEventListener('click', slide3Prev);
document.getElementById('slider3-next').addEventListener('click', slide3Next);
slide3Prev();


function slide3Prev()
{
    if (current3Index>0)
    {
        current3Index--;
        const sizes = document.getElementsByClassName('slider3-img')[1].getBoundingClientRect();
        const offset = current3Index*sizes.width;       
        console.log(sizes);
        document.getElementsByClassName('line')[0].style.transform=`translateX(${-offset}px)`;
    }
}
function slide3Next()
{
    if (current3Index<images.length-1)
    {
        current3Index++;
        const offset = current3Index*200;
        document.getElementsByClassName('line')[0].style.transform=`translateX(${-offset}px)`;
    }
}
console.log($('.line'));

$('.slickslide').slick(
    {
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true
    }
);